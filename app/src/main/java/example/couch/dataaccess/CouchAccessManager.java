package example.couch.dataaccess;

import android.content.Context;
import android.util.Log;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Emitter;
import com.couchbase.lite.Manager;
import com.couchbase.lite.Mapper;
import com.couchbase.lite.Reducer;
import com.couchbase.lite.View;
import com.couchbase.lite.android.AndroidContext;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Random;

import example.couch.models.Bus;
import example.couch.models.Car;
import example.couch.models.Truck;

/**
 * Created by Anton on 9/12/2014.
 */
public class CouchAccessManager {

    public static final String TRUCKS_VIEW = "trucks";
    public static final String BUSES_VIEW = "buses";
    public static final String CARS_VIEW = "cars";
    public static final String ALL_CARS_VIEW = "all_cars";

    private static volatile Database _database;

    public static Database getDatabase(Context context) {
        Database localInstance = _database;
        if (localInstance == null) {
            synchronized (CouchAccessManager.class) {
                localInstance = _database;
                if (localInstance == null)
                    _database = localInstance = initDatabase(context, localInstance);
            }
        }
        return localInstance;
    }

    private static Database initDatabase(Context context, Database localInstance) {
        Database result = null;
        try {
            result = new Manager(new AndroidContext(context.getApplicationContext()), Manager.DEFAULT_OPTIONS).getDatabase("cars");
            initViews(result);

        } catch (CouchbaseLiteException e) {
            Log.e("TAG", "Can't create DB", e);
        } catch (IOException e) {
            Log.e("TAG", "Can't create DB", e);
        }
        return result;
    }

    private static void initViews(Database database) {
        View trucks = database.getView(TRUCKS_VIEW);
        trucks.setMapReduce(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                String carType = (String) document.get("carType");
                if (Truck.class.getCanonicalName().equals(carType)) {
                    emitter.emit(document.get("id"), CarParser.fromMap(document));
                }
            }
        }, new TotalCountReducer(), "1");

        View buses = database.getView(BUSES_VIEW);
        buses.setMapReduce(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                String carType = (String) document.get("carType");
                if (Bus.class.getCanonicalName().equals(carType)) {
                    emitter.emit(document.get("id"), CarParser.fromMap(document));
                }
            }
        }, new TotalCountReducer(), "1");

        View cars = database.getView(CARS_VIEW);
        cars.setMapReduce(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                String carType = (String) document.get("carType");
                if (Car.class.getCanonicalName().equals(carType)) {
                    emitter.emit(document.get("id"), CarParser.fromMap(document));
                }
            }
        }, new TotalCountReducer(), "1");

        View allCars = database.getView(ALL_CARS_VIEW);
        allCars.setMapReduce(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                emitter.emit(document.get("id"), CarParser.fromMap(document));
            }
        }, new TotalCountReducer(), "1");

    }

    private static class TotalCountReducer implements Reducer {
        @Override
        public Object reduce(List<Object> keys, List<Object> values, boolean rereduce) {
            if (rereduce) {
                return View.totalValues(values);
            } else {
                return new Integer(values.size());
            }
        }
    }
}
