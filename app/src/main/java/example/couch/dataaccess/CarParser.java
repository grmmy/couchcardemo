package example.couch.dataaccess;

import com.google.gson.Gson;

import java.util.Map;

import example.couch.models.Car;

/**
 * Created by Anton on 9/12/2014.
 */
public class CarParser {
    public static Car fromMap(Map<String, Object> map) {
        String type = (String) map.get("type");
        if(type == null) return null;
        Class clazz = null;
        try {
            clazz = Class.forName(type);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        return (Car)gson.fromJson(map.toString(), clazz);
    }
    public static Map<String, Object> toMap(Car truck){
        Gson gson = new Gson();
        return gson.fromJson(gson.toJson(truck), Map.class);
    }
}
