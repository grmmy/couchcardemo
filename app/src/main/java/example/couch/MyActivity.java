package example.couch;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.LiveQuery;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.SavedRevision;
import com.couchbase.lite.util.Log;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import example.couch.dataaccess.CarParser;
import example.couch.dataaccess.CouchAccessManager;
import example.couch.models.Car;


public class MyActivity extends Activity {

    private LiveQuery _liveQuery;
    private ArrayAdapter<Car> _adapter;
    private Timer _timer;
    private TrafficJamSimulator _simulator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        final Database database = CouchAccessManager.getDatabase(this);
        _simulator = new TrafficJamSimulator(CouchAccessManager.getDatabase(this));
        initListView();
        initQuery(database);
    }

    private void initListView() {
        final ListView carList = (ListView) findViewById(R.id.car_list);
        _adapter = new ArrayAdapter<Car>(this, android.R.layout.simple_list_item_1, android.R.id.text1);
        carList.setAdapter(_adapter);
        carList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Car car = (Car)parent.getItemAtPosition(position);
                DetailActivity.callActivity(car.id, MyActivity.this);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        _timer = new Timer();
        _timer.schedule(new TimerTask() {
            @Override
            public void run() {
                final Random random = new Random();
                _simulator.next(random.nextInt(10));
            }
        }, 0, 1500);
    }

    @Override
    protected void onPause() {
        super.onPause();
        _timer.cancel();
    }

    private void initQuery(Database database) {
        final Query query = database.getView(CouchAccessManager.ALL_CARS_VIEW).createQuery();
        query.setMapOnly(true);
        _liveQuery = query.toLiveQuery();
        final LiveQuery.ChangeListener changeListener = new LiveQuery.ChangeListener() {
            @Override
            public void changed(LiveQuery.ChangeEvent event) {
                if (event.getSource().equals(_liveQuery)) {
                    final QueryEnumerator it = event.getRows();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            _adapter.clear();
                            while (it.hasNext()) {
                                final QueryRow carMap = it.next();
                                final SavedRevision currentRevision = carMap.getDocument().getCurrentRevision();
                                if (currentRevision != null) {
                                    _adapter.add(CarParser.fromMap(currentRevision.getProperties()));
                                }
                            }
                        }
                    });
                }
            }
        };
        _liveQuery.addChangeListener(changeListener);
        try {
            _liveQuery.run();
        } catch (CouchbaseLiteException e) {
            Log.e("Tag", "Can't run query", e);
        }
    }

}
