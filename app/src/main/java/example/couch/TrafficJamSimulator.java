package example.couch;

import android.graphics.Point;
import android.util.Log;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.LiveQuery;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.SavedRevision;
import com.couchbase.lite.UnsavedRevision;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import example.couch.dataaccess.CarParser;
import example.couch.dataaccess.CouchAccessManager;
import example.couch.models.Bus;
import example.couch.models.Car;
import example.couch.models.Truck;

/**
 * Created by Anton on 9/12/2014.
 */
public class TrafficJamSimulator {

    private final Random _random = new Random();
    private final Database _database;

    public TrafficJamSimulator(Database database) {
        _database = database;
    }

    public void next(int coef) {
        try {
            if (getTotalCarsCount() < 30) {
                if (coef < 3) {
                    addRandomTruck();
                } else if (coef < 7) {
                    addRandomBus();
                } else {
                    addRandomCar();
                }
            }
            List<Car> cars = getCars();
            for (Car car : cars) {
                if (getTotalCarsCount() >= 30 || (_random.nextBoolean() && getTotalCarsCount() > 25)) {
                    removeCar(car);
                    continue;
                }
                if (_random.nextBoolean()) {
                    if (car instanceof Truck) {
                        Truck currentTrack = ((Truck) car);
                        Truck newTruck = new Truck(currentTrack.id, currentTrack.year, currentTrack.passengers, getRandomCapacity());
                        updateCar(newTruck);
                    } else if (car instanceof Bus) {
                        Bus currentTrack = ((Bus) car);
                        Bus newTruck = new Bus(currentTrack.id, currentTrack.year, currentTrack.passengers, getRandomRoute());
                        updateCar(newTruck);
                    } else {
                        Car newCar = new Car(car.id, getRandomPassengers(), car.year);
                        updateCar(newCar);
                    }
                }
            }

        } catch (CouchbaseLiteException exception) {
            Log.e("Tag", "Can't do next step", exception);
        }
    }

    private void removeCar(Car car) throws CouchbaseLiteException {
        _database.getDocument(car.id).delete();
    }

    private Point[] getRandomRoute() {
        final int routLength = _random.nextInt(20) + 1;
        Point[] result = new Point[routLength];
        for (int pointIndex = 0; pointIndex < routLength; pointIndex++) {
            result[pointIndex] = new Point(_random.nextInt(1000), _random.nextInt(1000));
        }
        return result;
    }

    private void updateCar(Car car) throws CouchbaseLiteException {
        final Document carDocument = _database.getDocument(car.id);
        final Map<String, Object> carMap = CarParser.toMap(car);
        carMap.put("type", car.getClass().getCanonicalName());
        carDocument.update(new Document.DocumentUpdater() {
            @Override
            public boolean update(UnsavedRevision newRevision) {
                newRevision.setProperties(carMap);
                return true;
            }
        });
    }

    private double getRandomCapacity() {
        double capacity = _random.nextInt(100) / 1000;
        return capacity;
    }

    private void addRandomCar() throws CouchbaseLiteException {
        Car car = new Car(UUID.randomUUID().toString(), _random.nextInt(64) + 1950, _random.nextInt(3) + 1);
        addCarToDb(car);
    }

    private void addCarToDb(Car car) throws CouchbaseLiteException {
        Map<String, Object> carMap = CarParser.toMap(car);
        carMap.put("type", car.getClass().getCanonicalName());
        final Document carDocument = _database.getDocument(car.id);
        carDocument.putProperties(carMap);
    }

    private void addRandomBus() throws CouchbaseLiteException {
        Bus car = new Bus(UUID.randomUUID().toString(), _random.nextInt(64) + 1950, _random.nextInt(3) + 1, getRandomRoute());
        addCarToDb(car);
    }

    private void addRandomTruck() throws CouchbaseLiteException {
        Truck car = new Truck(UUID.randomUUID().toString(), _random.nextInt(64) + 1950, _random.nextInt(3) + 1, getRandomCapacity());
        addCarToDb(car);
    }

    public List<Car> getCars() {
        List<Car> result = new ArrayList<Car>();
        try {
            final Query query = _database.getView(CouchAccessManager.ALL_CARS_VIEW).createQuery();
            query.setMapOnly(true);
            final QueryEnumerator queryEnumerator = query.run();
            for (QueryEnumerator it = queryEnumerator; it.hasNext(); ) {
                QueryRow row = it.next();
                final SavedRevision currentRevision = row.getDocument().getCurrentRevision();
                if(currentRevision != null) {
                    result.add(CarParser.fromMap(currentRevision.getProperties()));
                }
            }
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int getRandomPassengers() {
        return _random.nextInt(4) + 1;
    }

    private int carsCount = -1;
    private LiveQuery _liveQuery;

    public Integer getTotalCarsCount() throws CouchbaseLiteException {
        if (carsCount != -1) {
            return carsCount;
        }
        _liveQuery = _database.getView(CouchAccessManager.ALL_CARS_VIEW).createQuery().toLiveQuery();
        _liveQuery.setMapOnly(true);
        _liveQuery.addChangeListener(new LiveQuery.ChangeListener() {
            @Override
            public void changed(LiveQuery.ChangeEvent event) {
                carsCount = event.getRows().getCount();
            }
        });
        final QueryEnumerator queryResult = _liveQuery.run();
        carsCount = queryResult.getCount();
        return carsCount;
    }
}
