package example.couch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.couchbase.lite.Document;
import com.couchbase.lite.SavedRevision;
import com.couchbase.lite.internal.RevisionInternal;

import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import example.couch.dataaccess.CarParser;
import example.couch.dataaccess.CouchAccessManager;
import example.couch.models.Bus;
import example.couch.models.Car;
import example.couch.models.Truck;


public class DetailActivity extends Activity {

    public static final String DOCUMENT_ID_EXTRA = "document_id";
    private Document _document;
    private TextView _passengers;
    private TextView _route;
    private TextView _capacity;
    private TextView _id;
    private TextView _type;
    private Switch _isDeleted;
    private LinearLayout _capacityBlock;
    private LinearLayout _routeBlock;
    private TextView _year;

    private Timer _timer;
    private TrafficJamSimulator _simulator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        _simulator = new TrafficJamSimulator(CouchAccessManager.getDatabase(this));
        initUI();
        String documentId = getIntent().getStringExtra(DOCUMENT_ID_EXTRA);
        _document = CouchAccessManager.getDatabase(this).getDocument(documentId);
        _document.addChangeListener(new Document.ChangeListener() {
            @Override
            public void changed(Document.ChangeEvent event) {
                final RevisionInternal addedRevision = event.getChange().getAddedRevision();
                final Map<String, Object> properties = addedRevision.getProperties();
                if(!addedRevision.isDeleted() && properties != null){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateUI(properties);
                        }
                    });
                } else {
                    if(addedRevision.isDeleted()){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                markObjectAsDeleted();
                            }
                        });
                    }
                }
            }
        });
        final SavedRevision currentRevision = _document.getCurrentRevision();
        if(currentRevision != null) {
            updateUI(currentRevision.getProperties());
        } else if(_document.isDeleted()) {
            markObjectAsDeleted();
        }
    }

    private void markObjectAsDeleted() {
        _isDeleted.setChecked(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        _timer = new Timer();
        _timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Random random = new Random();
                _simulator.next(random.nextInt(10));
            }
        }, 0, 500);
    }

    @Override
    protected void onPause() {
        super.onPause();
        _timer.cancel();
    }

    private void initUI() {
        _passengers = (TextView) findViewById(R.id.passangers);
        _route = (TextView) findViewById(R.id.route_points);
        _capacity = (TextView) findViewById(R.id.capacity);
        _id = (TextView) findViewById(R.id.id);
        _type = (TextView) findViewById(R.id.type);
        _isDeleted = (Switch) findViewById(R.id.deleted);
        _year = (TextView) findViewById(R.id.year);
        _capacityBlock = (LinearLayout) findViewById(R.id.capacity_block);
        _routeBlock = (LinearLayout) findViewById(R.id.route_block);
    }

    private void updateUI(Map<String, Object> properties) {
        final String type = (String) properties.get("type");

        Car car = CarParser.fromMap(properties);
        if(Truck.class.getCanonicalName().equals(type)){
            initUIForTruck((Truck) car);
        }
        if(Bus.class.getCanonicalName().equals(type)){
            initUIForBus((Bus) car);
        }
        if(Car.class.getCanonicalName().equals(type)){
            initUIForCar(car);
        }
        _type.setText(Car.class.getSimpleName());
        _id.setText(car.id);
        _passengers.setText(String.valueOf(car.passengers));
        _year.setText(String.valueOf(car.year));
    }

    private void initUIForCar(Car car) {
        _routeBlock.setVisibility(View.GONE);
        _capacityBlock.setVisibility(View.GONE);
    }

    private void initUIForBus(Bus bus) {
        _routeBlock.setVisibility(View.VISIBLE);
        _capacityBlock.setVisibility(View.GONE);
        _route.setText(String.valueOf(bus.route.length));
    }

    private void initUIForTruck(Truck truck) {
        _capacityBlock.setVisibility(View.VISIBLE);
        _routeBlock.setVisibility(View.GONE);
        _capacity.setText(String.valueOf(truck.capacity));
    }

    public static void callActivity(String carId, Activity caller){
        Intent intent = new Intent(caller, DetailActivity.class);
        intent.putExtra(DOCUMENT_ID_EXTRA, carId);
        caller.startActivity(intent);
    }

}
