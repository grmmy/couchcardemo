package example.couch.models;

/**
 * Created by Anton on 9/12/2014.
 */
public class Car {
    public final String id;
    public final int year;
    public final int passengers;

    public Car(String id, int year, int passengers) {
        this.id = id;
        this.year = year;
        this.passengers = passengers;
    }

    @Override
    public String toString() {
        return "I am a car " + id + " " + year + " " + ". I got " + passengers + " passengers";
    }
}
