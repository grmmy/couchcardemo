package example.couch.models;

import android.graphics.Point;

/**
 * Created by Anton on 9/12/2014.
 */
public class Bus extends Car {

    public final Point[] route;

    public Bus(String id, int year, int passengers, Point[] route) {
        super(id, year, passengers);
        this.route = route;
    }

    @Override
    public String toString() {
        return "I am a bus " + id + " " + year + " " + ". I got " + passengers + " passengers and " + route.length + " in my route";
    }
}
