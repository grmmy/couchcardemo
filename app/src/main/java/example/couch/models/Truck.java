package example.couch.models;

/**
 * Created by Anton on 9/12/2014.
 */
public class Truck extends Car {

    public final double capacity;

    public Truck(String id, int year, int passengers, double capacity) {
        super(id, year, passengers);
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "I am a truck " + id + " " + year + " " + ". I got " + passengers + " passengers and " + capacity + " capacity";
    }
}
